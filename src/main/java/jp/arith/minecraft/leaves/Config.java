package jp.arith.minecraft.leaves;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

public class Config {
	
	public final int searchRadius;
	public final int decayTimeMax;
	public final int decayTimeMin;

	private Config(Configuration cfg) {
		int searchRadius = 4;
		int decayTimeMax = 15;
		int decayTimeMin = 5;
		
		try
		{
			cfg.load();
			
			searchRadius = cfg.get("FastLeaves", "SearchRadius", searchRadius).getInt();
			decayTimeMax = cfg.get("FastLeaves", "DecayTimeMax", decayTimeMax).getInt();
			decayTimeMin = cfg.get("FastLeaves", "DecayTimeMin", decayTimeMin).getInt();

			FastLeaves.LOG.info("Loading config file has successfully completed.");
		}
		catch (Exception e)
		{
			FastLeaves.LOG.warn("Loading config file failed.");
		}
		finally
		{
			cfg.save();
			FastLeaves.LOG.info("Config file saved.");
			
			this.searchRadius = searchRadius;
			this.decayTimeMax = decayTimeMax;
			this.decayTimeMin = decayTimeMin;
		}
	}
	
	public static Config Load(File file) {
		FastLeaves.LOG.info("Config file: " + file.getPath());
		return new Config(new Configuration(file));
	}
	
}
