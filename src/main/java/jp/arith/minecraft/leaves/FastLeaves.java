package jp.arith.minecraft.leaves;

import java.util.Random;

import jp.arith.minecraft.Log;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = FastLeaves.MODID, version = FastLeaves.VERSION, name = FastLeaves.MODNAME)
public class FastLeaves {
    public static final String MODID = "Arith.FastLeaves";
    public static final String VERSION = "0.1.0";
    public static final String MODNAME = "FastLeaves";

    public static final Log LOG = new Log("FastLeaves");
    public static final Random RND = new Random();
    
    static Config config;
    
    public static Config getConfig() { return config; }
    
	@SidedProxy(clientSide = "jp.arith.minecraft.leaves.CommonProxy", 
			serverSide = "jp.arith.minecraft.leaves.CommonProxy")
	static CommonProxy proxy;
	public static CommonProxy getProxy() { return proxy; }
	
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	LOG.info("Init");
    	
    	getProxy().init();
    }
    

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)  {
    	LOG.info("PreInit");
    	
		LOG.debug("Debug output is not suppressed.");
		LOG.trace("Trace output is not suppressed.");
		
		config = Config.Load(event.getSuggestedConfigurationFile());
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)  {
    	LOG.info("PostInit");
	}
}
