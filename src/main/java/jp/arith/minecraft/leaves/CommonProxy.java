package jp.arith.minecraft.leaves;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class CommonProxy {
	public void init() {
		FMLCommonHandler.instance().bus().register(this);
    	MinecraftForge.EVENT_BUS.register(this);
	}
	
    @SubscribeEvent
    public void blockBreaked(BreakEvent event) {
    	Config cfg = FastLeaves.getConfig(); 
    	int r = cfg.searchRadius;
		int min = cfg.decayTimeMin, max = cfg.decayTimeMax;
    	int x0 = event.x, y0 = event.y, z0 = event.z;
    	
    	for(int x = x0 - r; x <= x0 + r; x++)
        	for(int y = y0 - r; y <= y0 + r; y++)
            	for(int z = z0 - r; z <= z0 + r; z++) {
            		Block b = event.world.getBlock(x, y, z);
            		if(b.isLeaves(event.world, event.x, event.y, event.z)) {
            			event.world.scheduleBlockUpdate(x, y, z, b,
            					min + (int)(rnd.nextDouble() * (max - min)));
            		}
            	}
    	
    }
    
    static final Random rnd = new Random();
}
