package jp.arith.minecraft;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log {
	public static final Logger LOGGER = LogManager.getLogger("jp.arith");
	
	String moduleName;
	
	public Log() { this(null); }
	
	public Log(String moduleName) {
		this.moduleName = moduleName;
		
	}
	
	String createMessage(String message) {
		if(moduleName == null) return message;
		return moduleName + ": " + message; 
	}
	
	void log(Level level, String message) {
		LOGGER.log(level, createMessage(message));
	}
	
	public void debug(String message) {
		log(Level.DEBUG, message);
	}
	
	public void error(String message) {
		log(Level.ERROR, message);
	}
	
	public void fatal(String message) {
		log(Level.FATAL, message);
	}
	
	public void info(String message) {
		log(Level.INFO, message);
	}
	
	public void trace(String message) {
		log(Level.TRACE, message);
	}

	public void warn(String message) {
		log(Level.WARN, message);
	}
}
